package com.allstate.payment.dao;

import com.allstate.payment.entities.Payment;

import java.util.List;

public interface PaymentDAO {
    Long rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> findAll();
}
