package com.allstate.payment.dao;

import com.allstate.payment.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentMongoImpl implements PaymentDAO{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Long rowcount() {
        Query query = new Query();
        return mongoTemplate.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria((Criteria.where("id").is(id)));
        return mongoTemplate.findOne(query, Payment.class);
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria((Criteria.where("type").is(type)));
        return mongoTemplate.find(query, Payment.class);
    }

    @Override
    public int save(Payment payment) {
        try {
            //changed to return void as save method returns the object and not an int
            mongoTemplate.save(payment);
            return 1;
        }
        catch (Exception e) {
            return 0;
        }
    }
    @Override
    public List<Payment> findAll() {
        return mongoTemplate.findAll(Payment.class);
    }
}
