package com.allstate.payment.rest;

import com.allstate.payment.entities.Payment;
import com.allstate.payment.services.IPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
    @Autowired
    private IPaymentService service;
    private static final Logger log = LoggerFactory.getLogger(PaymentController.class);

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public Long getTotal()
    {
        log.info("Calling rowcount method");
        return service.rowcount();
    }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Payment getIdByPath(@PathVariable("id") int id)
    {
        log.info("Invoking Find by ID");
        return service.findById(id);
    }

    @RequestMapping(value = "/findByType/{type}", method = RequestMethod.GET)
    public List<Payment> getTypeByPath(@PathVariable("type") String type)
    {
        log.info("Invoking Find by type");

        return service.findByType(type);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public int save(@RequestBody Payment payment)
    {
        log.info("Adding a new Payment");
        return service.save(payment);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Payment> getAll()
    {
        return service.all();
    }

    @RequestMapping(value ="/status", method = RequestMethod.GET)
    public String getStatus()
    {
        log.info("status called");
        return "Rest Api is running";
    }
}
