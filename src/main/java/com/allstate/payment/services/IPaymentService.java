package com.allstate.payment.services;

import com.allstate.payment.entities.Payment;

import java.util.List;

public interface IPaymentService {
    Long rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> all();
}
