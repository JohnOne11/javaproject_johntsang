package com.allstate.payment.services;

import com.allstate.payment.dao.PaymentDAO;
import com.allstate.payment.entities.Payment;
import com.allstate.payment.exceptions.PaymentExceptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentBs implements IPaymentService {
    @Autowired
    private PaymentDAO dao;

    @Override
    public Long rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id) {
        if (id > 0)
            return dao.findById(id);
        else
            throw new PaymentExceptions("id needs to be greater than 0");
    }

    @Override
    public List<Payment> findByType(String type) {
        if (type != null && !type.isEmpty())
            return dao.findByType(type);
        else
            throw new PaymentExceptions("Type is blank");
    }

    @Override
    public int save(Payment payment) {
        //changed to return void as save method returns the object and not an int
        return dao.save(payment);
    }

    @Override
    public List<Payment> all() {
        //changed to return void as save method returns the object and not an int
        return dao.findAll();
    }
}
