package com.allstate.payment.entities;

import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document
public class Payment {
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custid;

    public Payment(int id, Date paymentDate, String type, double amount, int custid) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custid = custid;
    }
    public Payment(){}

    public String toString(){
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MMM/yyyy");
        return "Id: " + this.id + "; Payment Date: " + formatDate.format(this.paymentDate) +
                "; Type: " + this.type + "; Amount: " + this.amount + "; Cust Id: " + this.custid;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }
}
