package com.allstate.payment.exceptions;

public class PaymentExceptions extends RuntimeException{
    private static final long serialVersionUID = 3538181174213580960L;

    public PaymentExceptions(String message)
    {
        super(message);
    }
}
