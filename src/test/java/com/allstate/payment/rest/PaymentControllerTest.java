package com.allstate.payment.rest;

import com.allstate.payment.PaymentApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

//launch Spring Boot Ap on a random port
@SpringBootTest(classes = PaymentApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {

    @LocalServerPort
    private int port;   //autowire the random port generated at runtime into port variable

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders httpHeader = new HttpHeaders();

    @Test
    public void testStatus() {
        HttpEntity<String> httpEntity = new HttpEntity<>(null, httpHeader);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                        "http://localhost:"+port+"/api/status",
                        HttpMethod.GET, httpEntity, String.class);
        Assertions.assertEquals("Rest Api is running", responseEntity.getBody());
    }
}
