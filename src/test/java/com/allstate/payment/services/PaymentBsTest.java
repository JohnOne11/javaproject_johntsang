package com.allstate.payment.services;

import com.allstate.payment.entities.Payment;
import com.allstate.payment.exceptions.PaymentExceptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PaymentBsTest {

    @Autowired
    private PaymentBs paymentBs;

    @Test
    public void testSave(){
        //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date payDate = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, 2020);
//        cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
//        cal.set(Calendar.DAY_OF_MONTH, 2);
//        Date payDate = cal.getTime();
        Payment payment = new Payment(9, payDate, "Deposit", 100, 12);
        paymentBs.save(payment);
        Assertions.assertNotNull(payment);
    }

    @Test
    public void testRowcount() {
        Long result = paymentBs.rowcount();
        Assertions.assertEquals(1, result);
    }

    @Test
    public void testFindById() {
        Payment payment = paymentBs.findById(9);
        Assertions.assertNotNull(payment);
    }

    @Test
    public void testFindByType() {
        List<Payment> paymentsList = paymentBs.findByType("Deposit");
        Assertions.assertTrue(paymentsList.size() > 0);
    }

    @Test
    public void testFindByIdError() {
        PaymentExceptions thrown = assertThrows(PaymentExceptions.class,
                        () -> paymentBs.findById(0));
        Assertions.assertEquals("id needs to be greater than 0", thrown.getMessage());
    }

    @Test
    public void testFindByTypeError() {
        PaymentExceptions thrown = assertThrows(PaymentExceptions.class,
                () -> paymentBs.findByType(null));
        Assertions.assertEquals("Type is blank", thrown.getMessage());
    }
}

