package com.allstate.payment.dao;

import com.allstate.payment.entities.Payment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentMongoImplTest {
    @Autowired
    PaymentDAO dao;
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @BeforeEach
//    public void cleanUp() {
//        for (String collectionName : mongoTemplate.getCollectionNames()) {
//            if (!collectionName.startsWith("system.")) {
//                mongoTemplate.dropCollection(collectionName);
//            }
//        }
//    }

    @Test
    public void testSave(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 2);
        Date payDate = cal.getTime();
        Payment payment = new Payment(9, payDate, "Deposit", 100, 12);
        dao.save(payment);
        Assertions.assertNotNull(payment);
    }

    @Test
    public void testRowcount() {
        Long result = dao.rowcount();
        Assertions.assertEquals(1, result);
    }

    @Test
    public void testFindById() {
        Payment payment = dao.findById(9);
        Assertions.assertNotNull(payment);
    }

    @Test
    public void testFindByType() {
        List<Payment> paymentsList = dao.findByType("Deposit");
        Assertions.assertTrue(paymentsList.size() > 0);
    }
}
