package com.allstate.payment.entities;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {
    @Test()
    public void testConstructor()
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date payDate = cal.getTime();
        Payment payment = new Payment(10, payDate , "Deposit", 100.0, 1);
        assertEquals(100.0, payment.getAmount());
    }
}
